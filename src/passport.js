
const passport = require('passport');
const { User } = require('./api/models');
const passportJWT = require('passport-jwt');
const { jwtSecret } = require('./config');
const JWTStrategy   = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const LocalStrategy = require('passport-local').Strategy;

module.exports = {
  initializePassport: () => {
    passport.use(
      new LocalStrategy({
        usernameField: 'email',
        passwordField: 'passwordHash'
      }, 
      (email, passwordHash, cb) => {
        return User
          .findOne({ email, passwordHash })
          .then(user => {
            if (!user) {
              return cb(null, false, { message: 'Incorrect email or password.' });
            }
            return cb(null, { id: user._id }, { message: 'Logged In Successfully' });
        })
        .catch(err => cb(err));
      }
    ));
    
    passport.use(
      new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: jwtSecret
      },
      (jwtPayload, cb) => User.findById(jwtPayload.id).then(user => cb(null, user)).catch(err => cb(err))
    ));
  }
}