const Router = require('express').Router;
const { Product, Category, Delivery, Invoice, Customer } = require('../../models');
const nodemailer = require('nodemailer');
const { gmailPass, gmailUser } = require('../../../config');
const productObjectHandler = require('../../models/Product/index').responseAdminObjectFromMongoInstance;
const productImageObjectHandler = require('../../models/Product/index').responseAdminImageObjectFromMongoInstance;
const categoryObjectHandler = require('../../models/Category/index').responseObjectFromMongoInstance;
const invoiceObjectHandler = require('../../models/Invoice/index').responseObjectFromMongoInstance;
const customerObjectHandler = require('../../models/Customer/index').responseObjectFromMongoInstance;
const deliveryObjectHandler = require('../../models/Delivery/index').responseObjectFromMongoInstance;
const router = Router();
const multer = require('multer');
const upload = multer({ dest: 'images' });
const mongoose = require('mongoose');
const sharp = require('sharp');
const fs = require('fs');
const pug = require('pug');
const path = require('path');
const myTemplate = pug.compileFile(path.join(__dirname, 'sent_mail_response_template.pug'));
const {
  responseSuccesful,
  dbErrorHandler,
  responseMissingField,
  responseNotSuccesful,
  imageUrlFromHash,
  responseInvalidField,
  sendMail
} = require('../../utils');

router.get('/product/list', (req, res) => {
  const { categoryId } = req.query;
  const findObject = {};

  if (categoryId) {
    if (!mongoose.Types.ObjectId.isValid(categoryId)) return responseInvalidField(res, 'categoryId');
    findObject.categoryId = categoryId;
  }

  Product.find(findObject)
    .then(products => responseSuccesful(res, { products: products.map(productObjectHandler) }))
    .catch(err => dbErrorHandler(res, err));
});

router.put('/product/:id', (req, res) => {
  const { id } = req.params;

  if (!id) return responseMissingField(res, 'id');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseInvalidField(res, 'id');

  const { code, name, siteName, purchasePrice, retailPrice, categoryId, description, discountPrice } = req.body;
  const updateObject = {};

  if (code) {
    if (!/^[0-9]{6}$/.test(code)) return responseInvalidField(res, 'code');
    updateObject.code = code;
  }

  if (name) updateObject.name = name;
  if (siteName) updateObject.siteName = siteName;

  if (purchasePrice) {
    if (isNaN(purchasePrice)) return responseInvalidField(res, 'purchasePrice');
    updateObject.purchasePrice = parseFloat(purchasePrice);
  }

  if (retailPrice) {
    if (isNaN(retailPrice)) return responseInvalidField(res, 'retailPrice');
    updateObject.retailPrice = parseFloat(retailPrice);
  }

  if (discountPrice !== null) {
    if (isNaN(discountPrice)) return responseInvalidField(res, 'discountPrice');
    if (discountPrice === '') {
      updateObject.discountPrice = null;
    } else {
      updateObject.discountPrice = parseFloat(discountPrice);
    }
  }

  if (categoryId) {
    if (!mongoose.Types.ObjectId.isValid(categoryId)) return responseInvalidField(res, 'categoryId');
    updateObject.categoryId = mongoose.Types.ObjectId(categoryId);
  }

  if (description) updateObject.description = description;

  Product.findOneAndUpdate({ _id: id }, updateObject, { new: true })
    .then(product => {
      responseSuccesful(res, { product: productObjectHandler(product) });
    })
    .catch(err => dbErrorHandler(res, err));
});

router.delete('/product/:id', (req, res) => {
  const { id } = req.params;

  if (!id) return responseMissingField(res, 'id');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseInvalidField(res, 'id');

  Product.findByIdAndDelete(id)
    .then(() => responseSuccesful(res))
    .catch(err => dbErrorHandler(res, err));
});

const isOrientationValid = orientation => [1, 2, 3, 4, 5, 6, 7, 8].includes(orientation);
const getImageCorespondingToOrientation = (image, orientation = -1) => {
  switch (orientation) {
    case -1:
    case 1:
    default:
      return image;
    case 2:
      return image.flip();
    case 3:
      return imager.rotate(180);
    case 4:
      return image.flip().rotate(180);
    case 5:
      return image.flip().rotate(-90);
    case 6:
      return image.rotate(90);
    case 7:
      return image.flip().rotate(90);
    case 8:
      return image.rotate(-90);
  }
};

router.post('/product/:id/image', upload.single('image'), (req, res) => {
  const { id } = req.params;
  const { imageId, orientation } = req.body;
  if (!id) return responseMissingField(res, 'id');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseInvalidField(res, 'id');

  let _orientation;
  if (orientation != null) {
    _orientation = parseInt(orientation);
    if (!isOrientationValid(_orientation)) return responseInvalidField(res, 'orientation');
  }
  if (!mongoose.Types.ObjectId.isValid(id)) return responseInvalidField(res, 'id');
  const { file } = req;
  if (!file) return responseMissingField(res, 'image');
  const imageHash = file.filename;
  const path = `images/${imageHash}`;
  const newPath = path + '.png';
  const imageNotRotated = sharp(path).resize(512, 512);

  getImageCorespondingToOrientation(imageNotRotated, _orientation)
    .toFile(newPath)
    .then(() => {
      Product.findById(id).then(product => {
        if (!product) return responseNotSuccesful(res, 'No product with given id');
        product.album.push({
          hash: imageHash,
          id: imageId
        });
        product.save((err, product) => {
          fs.unlinkSync(path);
          responseSuccesful(res, { photo: imageUrlFromHash(imageHash) });
        });
      });
    })
    .catch(err => dbErrorHandler(res, err));
});

router.put('/product/:id/album-order', (req, res) => {
  const { id } = req.params;
  const { order } = req.body;

  if (!id) return responseMissingField(res, 'id');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseInvalidField(res, 'id');

  if (!order) return responseMissingField(res, 'order');

  Product.findById(id)
    .then(product => {
      if (!product) return responseNotSuccesful(res, 'No product with given id');
      let error = false;
      const newProductAlbum = order.map(_id => {
        const albumObject = product.album.find(obj => obj.id === _id);
        if (albumObject == null) error = true;
        return albumObject;
      });
      if (error) return responseNotSuccesful(res, 'Not all IDs are presented at product album');

      product.album = newProductAlbum;
      product.save(() => {
        responseSuccesful(res, { album: product.album.map(productImageObjectHandler) });
      });
    })
    .catch(err => dbErrorHandler(res, err));
});

router.post('/product', (req, res) => {
  const { code, name, siteName, purchasePrice, retailPrice, categoryId, description, discountPrice } = req.body;

  if (!code) return responseMissingField(res, 'code');
  if (!/^[0-9]{6}$/.test(code)) return responseInvalidField(res, 'code');

  if (!name) return responseMissingField(res, 'name');
  if (!siteName) return responseMissingField(res, 'siteName');

  if (!purchasePrice) return responseMissingField(res, 'purchasePrice');
  const _purchasePrice = parseFloat(purchasePrice);
  if (isNaN(_purchasePrice)) return responseInvalidField(res, 'purchasePrice');

  if (!retailPrice) return responseMissingField(res, 'retailPrice');
  const _retailPrice = parseFloat(retailPrice);
  if (isNaN(_retailPrice)) return responseInvalidField(res, 'retailPrice');

  if (!categoryId) return responseMissingField(res, 'categoryId');
  if (!mongoose.Types.ObjectId.isValid(categoryId)) return responseInvalidField(res, 'categoryId');

  if (!description) return responseMissingField(res, 'description');
  const product = {
    code,
    name,
    siteName,
    currency: 'UAH',
    purchasePrice: _purchasePrice,
    retailPrice: _retailPrice,
    categoryId: mongoose.Types.ObjectId(categoryId),
    description
  };

  if (discountPrice) {
    if (isNaN(discountPrice)) return responseInvalidField(res, 'discountPrice');
    const _discountPrice = parseFloat(discountPrice);
    product.discountPrice = _discountPrice;
  }
  Product.create(product)
    .then(product => responseSuccesful(res, { product: productObjectHandler(product) }))
    .catch(err => dbErrorHandler(res, err));
});

router.post('/category', (req, res) => {
  const { name } = req.body;
  if (!name) return responseMissingField(res, 'name');
  const category = { name };
  Category.create(category)
    .then(cat => responseSuccesful(res, { category: categoryObjectHandler(cat) }))
    .catch(err => dbErrorHandler(res, err));
});

router.put('/category/:id', (req, res) => {
  const { id } = req.params;
  if (!id) return responseMissingField(res, 'id');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseInvalidField(res, 'id');

  const { name } = req.body;
  if (!name) return responseMissingField(res, 'name');

  Category.findByIdAndUpdate(id, { name }, { new: true })
    .then(category => {
      responseSuccesful(res, { category: categoryObjectHandler(category) });
    })
    .catch(err => dbErrorHandler(res, err));
});

router.delete('/category/:id', (req, res) => {
  const { id } = req.params;
  if (!id) return responseMissingField(res, 'id');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseInvalidField(res, 'id');

  Category.findByIdAndDelete(id)
    .then(() => responseSuccesful(res))
    .catch(err => dbErrorHandler(res, err));
});

const RETAIL_INVOICE_TYPE = 1;
const PURCHASE_INVOICE_TYPE = 2;

router.post('/invoice', async (req, res) => {
  const { type, products } = req.body;

  if (type == null) return responseMissingField(res, 'type');
  const _type = parseInt(type);
  if (_type !== PURCHASE_INVOICE_TYPE) return responseInvalidField(res, 'type');

  if (products == null) return responseMissingField(res, 'products');
  if (!Array.isArray(products)) return responseInvalidField(res, 'products');
  if (products.length === 0) return responseNotSuccesful(res, 'There should be at least one product in invoice');

  for (const i in products) {
    const product = products[i];
    if (!product.id) return responseMissingField(res, `id in products[${i}]`);
    if (!mongoose.Types.ObjectId.isValid(product.id)) return responseInvalidField(res, `id in products[${i}]`);

    if (product.quantity == null) return responseMissingField(res, `quantity in products[${i}]`);
    if (isNaN(product.quantity)) return responseInvalidField(res, `quantity in products[${i}]`);

    if (product.price != null) {
      if (isNaN(product.price)) return responseInvalidField(res, `price in products[${i}]`);
    }
  }

  const invoice = {
    createDate: Date.now(),
    type: _type,
    products: products.map(prod => ({
      productId: mongoose.Types.ObjectId(prod.id),
      quantity: Number(prod.quantity),
      price: prod.price != null ? Number(prod.price) : null
    }))
  };

  const prodPriceById = {};
  const productsUpdatePromises = [];

  for (let j = 0; j < products.length; j++) {
    const prod = products[j];
    productsUpdatePromises.push(
      Product.findByIdAndUpdate(prod.id, { $inc: { balance: prod.quantity } }).then(
        _prod => (prodPriceById[prod.id] = _prod.purchasePrice)
      )
    );
  }
  await Promise.all(productsUpdatePromises);

  invoice.products = invoice.products.map(prod => ({
    ...prod,
    price: prod.price !== null ? prod.price : prodPriceById[prod.productId]
  }));

  Invoice.create(invoice)
    .then(invoice => responseSuccesful(res, { invoice: invoiceObjectHandler(invoice) }))
    .catch(err => dbErrorHandler(res, err));
});

router.get('/product/:id/feedback/:feedbackId/toggle-visibility', (req, res) => {
  const { id, feedbackId } = req.params;

  if (!id) return responseNotSuccesful(res, 'Parametr id required');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseNotSuccesful(res, 'Given id is not valid ObjectId');

  if (!feedbackId) return responseNotSuccesful(res, 'Parametr feedbackId required');
  if (!mongoose.Types.ObjectId.isValid(feedbackId))
    return responseNotSuccesful(res, 'Given feedbackId is not valid ObjectId');

  Product.findById(id)
    .then(product => {
      if (!product) return responseNotSuccesful(res, 'Product with given id not found');
      const index = product.feedbacks.findIndex(fb => fb._id == feedbackId);
      if (index === -1) return responseNotSuccesful(res, 'Feedback with given id not found');
      const feedbacks = product.feedbacks.toObject();
      product.feedbacks = [
        ...feedbacks.slice(0, index),
        {
          ...feedbacks[index],
          visible: !feedbacks[index].visible
        },
        ...feedbacks.slice(index + 1)
      ];

      product.save();
      responseSuccesful(res);
    })
    .catch(err => dbErrorHandler(res, err));
});

router.put('/delivery/:id/sent-to-customer', (req, res) => {
  const { id } = req.params;
  if (!id) return responseMissingField(res, 'id');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseInvalidField(res, 'id');

  const { declarationNumber } = req.body;

  if (!declarationNumber) return responseMissingField(res, 'declarationNumber');

  Delivery.findByIdAndUpdate(id, { sentToCustomer: Date.now(), declarationNumber }, { new: true })
    .then(delivery => {
      Customer.findById(delivery.customerId)
        .then(customer => {
          if (customer === null) {
            return responseNotSuccesful(res, 'Invalid customerId in delivery record');
          }
          sendMail(myTemplate({ delivery, customer }), customer.email);
          responseSuccesful(res, { delivery: deliveryObjectHandler(delivery) });
        })
        .catch(err => dbErrorHandler(res, err));
    })
    .catch(err => dbErrorHandler(res, err));
});

router.put('/invoice/:id/cancel', async (req, res) => {
  const { id } = req.params;
  if (!id) return responseMissingField(res, 'id');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseInvalidField(res, 'id');
  try {
    const invoice = await Invoice.findById(id);
    const products = [];
    for (let i = 0; i < invoice.products.length; i++) {
      const product = await Product.findById(invoice.products[i].productId);
      const { quantity } = invoice.products[i];
      if (invoice.type === PURCHASE_INVOICE_TYPE && product.balance < quantity) {
        return responseNotSuccesful(res, 'Not enough products to cancel invoice');
      }
      products.push({
        mongoInstance: product,
        quantityToAdd: invoice.type === PURCHASE_INVOICE_TYPE ? -1 * quantity : quantity
      });
    }

    const productsUpdatePromises = [];

    for (let j = 0; j < products.length; j++) {
      const product = products[j];
      product.mongoInstance.balance = product.mongoInstance.balance + product.quantityToAdd;
      productsUpdatePromises.push(product.mongoInstance.save());
    }

    await Promise.all(productsUpdatePromises);

    invoice.canceled = Date.now();
    invoice.save((err, inv) => {
      if (err) {
        return dbErrorHandler(res, err);
      }
      responseSuccesful(res, { invoice: invoiceObjectHandler(inv) });
    });
  } catch (err) {
    dbErrorHandler(res, err);
  }
});

// router.put('/invoice/:id/paid', (req, res) => {
//   const { id } = req.params;
//   if (!id) return responseMissingField(res, 'id');
//   if (!mongoose.Types.ObjectId.isValid(id)) return responseInvalidField(res, 'id');

//   Invoice.findByIdAndUpdate(id, { paid: Date.now() }, { new: true })
//     .then(invoice => responseSuccesful(res, { invoice: invoiceObjectHandler(invoice) }))
//     .catch(err => dbErrorHandler(res, err));
// });

router.get('/invoice/list', (req, res) => {
  Invoice.find()
    .populate('liqpayOrderId')
    .then(invoices => responseSuccesful(res, { invoices: invoices.map(invoiceObjectHandler) }))
    .catch(err => dbErrorHandler(res, err));
});

router.get('/customer/list', (req, res) => {
  Customer.find()
    .then(customers => responseSuccesful(res, { customers: customers.map(customerObjectHandler) }))
    .catch(err => dbErrorHandler(res, err));
});

router.get('/delivery/list', (req, res) => {
  Delivery.find()
    .then(deliveries => responseSuccesful(res, { deliveries: deliveries.map(deliveryObjectHandler) }))
    .catch(err => dbErrorHandler(res, err));
});

module.exports = { router };
