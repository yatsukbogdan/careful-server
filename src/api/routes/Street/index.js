const Router = require('express').Router;
const { Street } = require('../../models');
const mongoose = require('mongoose');
const { responseObjectFromMongoInstance } = require('../../models/Street/index');
const router = Router();
const { responseSuccesful, dbErrorHandler, responseNotSuccesful} = require('../../utils');

router.get('/list', (req, res) => {
  const findObject = {};
  const { cityId } = req.query;

  if (cityId) {
    if (!mongoose.Types.ObjectId.isValid(cityId)) return responseNotSuccesful(res, 'Given id is not valid ObjectId');
    else findObject.cityId = cityId
  }

  Street
    .find(findObject)
    .then(streets => responseSuccesful(res, { streets: streets.map(responseObjectFromMongoInstance) }))
    .catch(err => dbErrorHandler(res, err));
})

module.exports = { router };