const productRouter = require('./Product').router;
const countryRouter = require('./Country').router;
const cityRouter = require('./City').router;
const invoiceRouter = require('./Invoice').router;
const rozetkaRouter = require('./Rozetka').router;
const deliveryRouter = require('./Delivery').router;
const userRouter = require('./User').router;
const adminRouter = require('./Admin').router;
const categoryRouter = require('./Category').router;
const customerRouter = require('./Customer').router;
const streetRouter = require('./Street').router;
const novaPoshtaAdressRouter = require('./NovaPoshtaAdress').router;
const liqpayRouter = require('./LiqPay').router;

module.exports = {
  adminRouter,
  userRouter,
  categoryRouter,
  rozetkaRouter,
  deliveryRouter,
  customerRouter,
  invoiceRouter,
  productRouter,
  countryRouter,
  cityRouter,
  streetRouter,
  novaPoshtaAdressRouter,
  liqpayRouter
};
