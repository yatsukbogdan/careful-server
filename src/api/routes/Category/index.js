const Router = require('express').Router;
const { Category } = require('../../models');
const { responseObjectFromMongoInstance } = require('../../models/Category/index');
const router = Router();
const { responseSuccesful, dbErrorHandler, responseMissingField } = require('../../utils');

router.get('/list', (req, res) => {
  Category
    .find()
    .then(categories => responseSuccesful(res, { categories: categories.map(responseObjectFromMongoInstance) }))
    .catch(err => dbErrorHandler(res, err));
})

module.exports = { router };