const Router = require('express').Router;
const { Country } = require('../../models');
const { responseObjectFromMongoInstance } = require('../../models/Country/index');
const router = Router();
const { responseSuccesful, dbErrorHandler } = require('../../utils');

router.get('/list', (req, res) => {
  Country
    .find()
    .then(countries => responseSuccesful(res, { countries: countries.map(responseObjectFromMongoInstance) }))
    .catch(err => dbErrorHandler(res, err));
})

module.exports = { router };