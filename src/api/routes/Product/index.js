const Router = require('express').Router;
const { Product } = require('../../models');
const mongoose = require('mongoose');
const { responseObjectFromMongoInstance } = require('../../models/Product/index');
const router = Router();
const {
  responseNotSuccesful,
  responseSuccesful,
  dbErrorHandler,
  responseInvalidField,
  responseMissingField
} = require('../../utils');

router.get('/list', (req, res) => {
  const { categoryId } = req.query;
  const findObject = {};

  if (categoryId) {
    if (!mongoose.Types.ObjectId.isValid(categoryId)) return responseInvalidField(res, 'categoryId');
    findObject.categoryId = categoryId;
  }

  Product.find(findObject)
    .then(products => responseSuccesful(res, { products: products.map(responseObjectFromMongoInstance) }))
    .catch(err => dbErrorHandler(res, err));
});

router.get('/:id', (req, res) => {
  const { id } = req.params;

  if (!id) return responseNotSuccesful(res, 'Parametr id required');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseNotSuccesful(res, 'Given id is not valid ObjectId');

  Product.findById(id)
    .then(product => {
      if (!product) return responseNotSuccesful(res, 'Product with given id not found');
      responseSuccesful(res, { product: responseObjectFromMongoInstance(product) });
    })
    .catch(err => dbErrorHandler(res, err));
});

router.put('/:id/feedback', (req, res) => {
  const { id } = req.params;

  if (!id) return responseNotSuccesful(res, 'Parametr id required');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseNotSuccesful(res, 'Given id is not valid ObjectId');

  const { name, text, customerId } = req.body;

  if (!name) return responseMissingField(res, 'name');
  if (!text) return responseMissingField(res, 'text');
  let _customerId = null;
  console.log(customerId);
  if (customerId && mongoose.Types.ObjectId.isValid(customerId)) _customerId = customerId;

  Product.findById(id)
    .then(product => {
      if (!product) return responseNotSuccesful(res, 'Product with given id not found');
      product.feedbacks.push({
        name,
        text,
        customerId: _customerId,
        date: Date.now(),
        visible: false
      });
      product.save();
      responseSuccesful(res);
    })
    .catch(err => dbErrorHandler(res, err));
});

module.exports = { router };
