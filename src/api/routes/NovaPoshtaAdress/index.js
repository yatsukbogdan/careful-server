const Router = require('express').Router;
const { NovaPoshtaAdress } = require('../../models');
const mongoose = require('mongoose');
const { responseObjectFromMongoInstance } = require('../../models/NovaPoshtaAdress/index');
const router = Router();
const { responseSuccesful, dbErrorHandler, responseNotSuccesful} = require('../../utils');

router.get('/list', (req, res) => {
  const findObject = {};
  const { cityId } = req.query;

  if (cityId) {
    if (!mongoose.Types.ObjectId.isValid(cityId)) return responseNotSuccesful(res, 'Given id is not valid ObjectId');
    else findObject.cityId = cityId
  }

  NovaPoshtaAdress
    .find(findObject)
    .then(novaPoshtaAdresses => responseSuccesful(res, { novaPoshtaAdresses: novaPoshtaAdresses.map(responseObjectFromMongoInstance) }))
    .catch(err => dbErrorHandler(res, err));
})

module.exports = { router };