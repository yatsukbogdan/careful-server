const Router = require('express').Router;
const { adressFromDeliveryObject, sendMail, sendServiceMail } = require('../../utils');
const { LiqpayOrder, Invoice, Customer, Delivery, Product } = require('../../models');
const router = Router();
const pug = require('pug');
const path = require('path');
const userMail = pug.compileFile(path.join(__dirname, 'mail_response_template.pug'));
const serviceMail = pug.compileFile(path.join(__dirname, 'service_mail_template.pug'));

const mailPayloadFromData = data => {
  const { invoice, products, customer, delivery } = data;

  const _delivery = {
    id: delivery._id,
    adress: adressFromDeliveryObject(delivery),
    method: delivery.method === 0 ? 'Новая почта' : 'Укрпочта'
  };
  const _invoice = {
    products: invoice.products.map(prod => {
      const product = products.find(_prod => _prod.id == prod.productId);
      if (!product)
        return {
          name: 'unknown',
          price: 0,
          quantity: 0
        };

      return {
        name: product.siteName,
        price: product.retailPrice,
        discountPrice: product.discountPrice,
        quantity: prod.quantity
      };
    })
  };

  const _customer = {
    firstName: customer.firstName,
    lastName: customer.lastName
  };

  const prices = _invoice.products.reduce(
    (_prices, product) => ({
      normal: _prices.normal + product.price * product.quantity,
      discount: product.discountPrice
        ? _prices.discount + product.discountPrice * product.quantity
        : _prices.discount + product.price * product.quantity
    }),
    {
      normal: 0,
      discount: 0
    }
  );

  return {
    discountAmount: prices.normal - prices.discount,
    totalPrice: prices.discount,
    delivery: _delivery,
    invoice: _invoice,
    customer: _customer
  };
};

router.post('/', (req, res) => {
  const { data, signature } = req.body;
  console.log(data);
  const dataDecoded = JSON.parse(Buffer.from(data, 'base64').toString());
  const order = {
    paymentId: dataDecoded.payment_id,
    status: dataDecoded.status,
    liqpayOrderId: dataDecoded.liqpay_order_id,
    amount: dataDecoded.amount,
    currency: dataDecoded.currency,
    receiverCommission: dataDecoded.receiver_commission,
    createDate: dataDecoded.create_date,
    transactionId: dataDecoded.transaction_id,
    signature
  };
  LiqpayOrder.create(order)
    .then(_order => {
      Delivery.findById(dataDecoded.order_id)
        .then(async delivery => {
          try {
            const invoice = await Invoice.findByIdAndUpdate(
              delivery.invoiceId,
              { liqpayOrderId: _order._id },
              { new: true }
            ).exec();
            const products = await Product.find({ _id: { $in: invoice.products.map(prod => prod.productId) } }).exec();
            const customer = await Customer.findById(delivery.customerId).exec();

            if (!customer) {
              console.log('No customer with given id');
              return;
            }
            if (
              dataDecoded.status === 'success' ||
              dataDecoded.status === 'sandbox' ||
              dataDecoded.status === 'wait_accept'
            ) {
              const mailPayload = mailPayloadFromData({ delivery, customer, invoice, products });
              sendMail(userMail(mailPayload), customer.email);
              sendServiceMail(serviceMail(mailPayload));
            }
            res.json({ success: true });
          } catch (err) {
            console.log(err);
          }
        })
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
});

module.exports = { router };
