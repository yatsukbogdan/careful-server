const Router = require('express').Router;
const { Invoice, Product, LiqpayOrder } = require('../../models');
const mongoose = require('mongoose');
const router = Router();
const {
  responseSuccesful,
  dbErrorHandler,
  responseNotSuccesful,
  responseMissingField,
  responseInvalidField
} = require('../../utils');

const RETAIL_INVOICE_TYPE = 1;

router.post('/', async (req, res) => {
  const { type, products } = req.body;

  if (type == null) return responseMissingField(res, 'type');
  const _type = parseInt(type);
  if (_type !== RETAIL_INVOICE_TYPE) return responseInvalidField(res, 'type');

  if (products == null) return responseMissingField(res, 'products');
  if (!Array.isArray(products)) return responseInvalidField(res, 'products');
  if (products.length === 0) return responseNotSuccesful(res, 'There should be at least one product in invoice');

  const notEnoughProductsIds = [];
  const prodPriceById = {};

  for (const i in products) {
    const product = products[i];
    if (!product.id) return responseMissingField(res, `id in products[${i}]`);
    if (!mongoose.Types.ObjectId.isValid(product.id)) return responseInvalidField(res, `id in products[${i}]`);

    if (product.quantity == null) return responseMissingField(res, `quantity in products[${i}]`);
    if (isNaN(product.quantity)) return responseInvalidField(res, `quantity in products[${i}]`);

    const _prod = await Product.findById(product.id);

    try {
      prodPriceById[product.id] = _prod.discountPrice !== null ? _prod.discountPrice : _prod.retailPrice;
      if (_prod.balance < product.quantity) {
        notEnoughProductsIds.push(product.id);
      }
    } catch (err) {
      dbErrorHandler(res, err);
      return;
    }
  }

  if (notEnoughProductsIds.length > 0)
    return responseNotSuccesful(res, 'Not enough products', { productsIds: notEnoughProductsIds, code: 400001 });

  const productsUpdatePromises = [];
  for (let j = 0; j < products.length; j++) {
    const prod = products[j];
    productsUpdatePromises.push(Product.findByIdAndUpdate(prod.id, { $inc: { balance: -prod.quantity } }));
  }
  await Promise.all(productsUpdatePromises);

  const invoice = {
    createDate: Date.now(),
    type: _type,
    products: products.map(prod => ({
      productId: mongoose.Types.ObjectId(prod.id),
      quantity: Number(prod.quantity),
      price: prodPriceById[prod.id]
    }))
  };

  Invoice.create(invoice)
    .then(invoice => responseSuccesful(res, { id: invoice._id }))
    .catch(err => dbErrorHandler(res, err));
});

const errMessage =
  'Ваш заказ пока не обработан. Попробуйте обновить страничку через несколько минут. Если заказ не будет обработан в течение 5 минут, напишите на почту carefulstore.office@gmail.com. Спасибо за понимание! Если Вы отменили оплату, проигнорируйте это сообщение.';

router.get('/:id/status', (req, res) => {
  const { id } = req.params;

  if (!id) return responseMissingField(res, 'id');
  if (!mongoose.Types.ObjectId.isValid(id)) return responseInvalidField(res, 'id');

  Invoice.findById(id)
    .then(invoice => {
      if (!invoice) return responseNotSuccesful(res, 'There is no invoice with given id');
      if (!invoice.liqpayOrderId) return responseNotSuccesful(res);
      LiqpayOrder.findById(invoice.liqpayOrderId)
        .then(order => {
          if (!order) return responseNotSuccesful(res);
          if (order.status === 'wait_accept' || order.status === 'sandbox' || order.status === 'success') {
            return responseSuccesful(res);
          }
          return responseNotSuccesful(res);
        })
        .catch(err => dbErrorHandler(res, err));
    })
    .catch(err => dbErrorHandler(res, err));
});

module.exports = { router };
