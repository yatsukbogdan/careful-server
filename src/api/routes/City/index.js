const Router = require('express').Router;
const { City } = require('../../models');
const mongoose = require('mongoose');
const { responseObjectFromMongoInstance } = require('../../models/City/index');
const router = Router();
const { responseSuccesful, dbErrorHandler, responseNotSuccesful} = require('../../utils');

router.get('/list', (req, res) => {
  const findObject = {};
  const { countryId } = req.query;

  if (countryId) {
    if (!mongoose.Types.ObjectId.isValid(countryId)) return responseNotSuccesful(res, 'Given id is not valid ObjectId');
    else findObject.countryId = countryId
  }
  City
    .find(findObject)
    .then(cities => responseSuccesful(res, { cities: cities.map(responseObjectFromMongoInstance) }))
    .catch(err => dbErrorHandler(res, err));
})

module.exports = { router };