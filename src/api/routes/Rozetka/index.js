const Router = require('express').Router;
const router = Router();
const { responseSuccesful, responseNotSuccesful, responseMissingField } = require('../../utils');
const axios = require('axios');

const mapRozetkaCities = city => ({ id: city.id, text: city.title })
const mapRozetkaAdresses = adress => ({ id: adress.id, text: adress.title })

router.get('/cities', (req, res) => {
  const { name } = req.query;

  if (name === null) return responseMissingField(res, 'name');

  axios
    .get(encodeURI(`https://my.rozetka.com.ua/suggest/action=getLocalitiesForSuggestJSON/?name=${ name }`))
    .then(({data}) => {
      if (data.code !== 1) return responseNotSuccesful(res, 'Something went wrong');
      responseSuccesful(res, {
        cities: data.content.records.map(mapRozetkaCities)
      })
    })
})

router.get('/streets', (req, res) => {
  const { cityId, name } = req.query;

  if (name === null) return responseMissingField(res, 'name');
  if (cityId === null) return responseMissingField(res, 'cityId');  
  console.log(cityId);
  console.log(name)
  console.log(encodeURI(`https://my.rozetka.com.ua/suggest/action=getStreetsForSuggestJSON/?locality_id=${ cityId }&title=${ name }`));
  axios
    .get(encodeURI(`https://my.rozetka.com.ua/suggest/action=getStreetsForSuggestJSON/?locality_id=${ cityId }&title=${ name }`))
    .then(({data}) => {
      if (data.code !== 1) return responseNotSuccesful(res, 'Something went wrong');
      responseSuccesful(res, {
        streets: data.content.records.map(mapRozetkaAdresses)
      })
    })
})

module.exports = { router };