const Router = require('express').Router;
const { Customer } = require('../../models');
const router = Router();
const { nameValidation, surnameValidation, emailValidation, phoneValidation } = require('./validation');
const { responseSuccesful, dbErrorHandler, responseNotSuccesful, responseMissingField } = require('../../utils');

router.post('/', (req, res) => {
  const { firstName, lastName, email, phone } = req.body;

  if (!firstName) return responseMissingField(res, 'firstName');
  const nameValObj = nameValidation(firstName);
  if (!nameValObj.success) return responseNotSuccesful(res, nameValObj.error);

  if (!lastName) return responseMissingField(res, 'lastName');
  const lastNameValObj = surnameValidation(lastName);
  if (!lastNameValObj.success) return responseNotSuccesful(res, lastNameValObj.error);

  if (!email) return responseMissingField(res, 'email');
  const emailValObj = emailValidation(email);
  if (!emailValObj.success) return responseNotSuccesful(res, emailValObj.error);

  if (!phone) return responseMissingField(res, 'phone');
  const phoneValObj = phoneValidation(phone);
  if (!phoneValObj.success) return responseNotSuccesful(res, phoneValObj.error);

  const customer = {
    firstName,
    lastName,
    email,
    phone
  }

  Customer
    .create(customer)
    .then(cust => responseSuccesful(res, { id: cust._id }))
    .catch(err => dbErrorHandler(res, err));
})

module.exports = { router };