const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PHONE_REGEX = /^(\+?38)?[0-9]{10}$/;

const nameValidation = name => {
  if (name.length === 0) return { success: false, error: 'Поле Имя не может быть пустым' };
  if (name.length > 0 && name.length < 3) return { success: false, error: 'Имя не правильное' };
  return { success: true };
}

const surnameValidation = surname => {
  if (surname.length === 0) return { success: false, error: 'Поле Фамилия не может быть пустым' };
  if (surname.length > 0 && surname.length < 3) return { success: false, error: 'Фамилия не правильная' };
  return { success: true };
}

const emailValidation = email => {
  if (email.length === 0) return { success: false, error: 'Поле электронная почта не может быть пустым' };
  if (!EMAIL_REGEX.test(email.toLowerCase())) return { success: false, error: 'Электронная почта введена не правильно' };
  return { success: true };
}

const phoneValidation = phone => {
  if (phone.length === 0) return { success: false, error: 'Поле номер телефона не может быть пустым' };
  if (!PHONE_REGEX.test(phone.toLowerCase())) return { success: false, error: 'Номер телефона введен не правильно' };
  return { success: true };
}

module.exports = {
  nameValidation,
  surnameValidation,
  emailValidation,
  phoneValidation
}