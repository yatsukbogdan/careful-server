const Router = require('express').Router;
const mongoose = require('mongoose');
const { Delivery } = require('../../models');
const router = Router();
const {
  responseSuccesful,
  dbErrorHandler,
  responseNotSuccesful,
  responseMissingField,
  responseInvalidField
} = require('../../utils');

router.post('/', (req, res) => {
  const { invoiceId, customerId, method, countryId, city, deliverySpecificData } = req.body;

  if (!invoiceId) return responseMissingField(res, 'invoiceId');
  if (!mongoose.Types.ObjectId.isValid(invoiceId)) return responseInvalidField(res, 'invoiceId');

  if (!customerId) return responseMissingField(res, 'customerId');
  if (!mongoose.Types.ObjectId.isValid(customerId)) return responseInvalidField(res, 'customerId');

  if (method === null) return responseMissingField(res, 'method');
  const _method = Number(method);
  if (_method !== 0 && _method !== 1) return responseInvalidField(res, 'method');

  if (!countryId) return responseMissingField(res, 'countryId');
  if (!mongoose.Types.ObjectId.isValid(countryId)) return responseInvalidField(res, 'countryId');

  if (!city) return responseMissingField(res, 'city');

  const { id, name, source } = city;
  if (id === null) return responseMissingField(res, 'id in city');
  if (!name) return responseMissingField(res, 'name in city');
  if (!source) return responseMissingField(res, 'source in city');
  if (source !== 'rozetka' && source !== 'novaPoshta') return responseInvalidField(res, 'source in city');

  if (!deliverySpecificData) return responseMissingField(res, 'deliverySpecificData');
  if (typeof deliverySpecificData !== 'object') return responseInvalidField(res, 'deliverySpecificData');

  if (_method === 0) {
    const { nppId, nppName } = deliverySpecificData;

    if (!nppId) return responseMissingField(res, 'nppId in deliverySpecificData');
    if (!nppName) return responseMissingField(res, 'nppName in deliverySpecificData');
  } else {
    const { streetName, house, apartment, postcode, streetId } = deliverySpecificData;

    if (!streetName) return responseMissingField(res, 'streetName in deliverySpecificData');
    if (!streetId) return responseMissingField(res, 'streetId in deliverySpecificData');
    if (!house) return responseMissingField(res, 'house in deliverySpecificData');
    if (!apartment) return responseMissingField(res, 'apartment in deliverySpecificData');
    if (!postcode) return responseMissingField(res, 'postcode in deliverySpecificData');
    if (!/^[0-9]{5}$/.test(postcode)) return responseInvalidField(res, 'postcode in deliverySpecificData');
  }

  const delivery = {
    invoiceId: mongoose.Types.ObjectId(invoiceId),
    customerId: mongoose.Types.ObjectId(customerId),
    method: _method,
    countryId: mongoose.Types.ObjectId(countryId),
    status: 0,
    city: {
      id,
      name,
      source
    },
    deliverySpecificData: {}
  };

  if (_method === 0) {
    const { nppId, nppName, cityId } = deliverySpecificData;
    delivery.deliverySpecificData = { nppId, nppName, cityId };
  } else {
    const { streetId, streetName, house, apartment, postcode } = deliverySpecificData;
    delivery.deliverySpecificData = { streetId, streetName, house, apartment, postcode };
  }

  Delivery.create(delivery)
    .then(del => responseSuccesful(res, { id: del._id }))
    .catch(err => dbErrorHandler(res, err));
});

module.exports = { router };
