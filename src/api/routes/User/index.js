
const Router = require('express').Router;
const { User } = require('../../models');
const passport = require('passport');
const multer  = require('multer');
const router = Router();
const jwt = require('jsonwebtoken');
const { jwtSecret, appId } = require('../../../config');
const { responseSuccesful } = require('../../utils');


router.post('/login', (req, res, next) => passportAuthenticate(req, res));

const passportAuthenticate = (req, res) => {
  passport.authenticate('local', { session: false }, (err, user, info) => {
    if (!user) {
      return res.status(200).json({
        success: false,
        message: info.message
      });
    }
    req.login(user, { session: false }, (err) => {
      if (err) return res.send(err);

      const token = jwt.sign(user, jwtSecret);
      User
        .findById(user.id)
        .then(() => responseSuccesful(res, { token }))
        .catch(err => {
          res.status(400).json({
            success: false,
            message: 'Something went wrong'
          })
        })
    });
  })(req, res);
}

module.exports = { router };