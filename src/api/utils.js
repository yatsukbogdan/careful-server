const { imagesHost, gmailPass, gmailUser } = require('../config');
const nodemailer = require('nodemailer');

const responseSuccesful = (res, payload) => res.json({ success: true, payload });
const dbErrorHandler = (res, err) => {
  responseNotSuccesful(res, 'Something went wrong with db');
  console.log(err);
};
const responseMissingField = (res, fieldName) => res.json({ success: false, message: `Missing field ${fieldName}` });
const responseInvalidField = (res, fieldName) => res.json({ success: false, message: `Invalid field ${fieldName}` });
const responseNotSuccesful = (res, message, payload = {}) => res.json({ success: false, message, payload });
const imageUrlFromHash = hash => `${imagesHost}/${hash}.png`;
const defaultGenderPhoto = gender => `${imagesHost}/${gender === 'm' ? 'man' : 'woman'}.jpg`;
const chatIdFromUsersIds = (userId1, userId2) => {
  if (userId1 > userId2) return userId1 + userId2;
  return userId2 + userId1;
};

const adressFromDeliveryObject = delivery => {
  if (delivery.method === 0) {
    return `г. ${delivery.city.name}, Отделение ${delivery.deliverySpecificData.nppName}`;
  }
  return `г. ${delivery.city.name}, ${delivery.deliverySpecificData.streetName}, дом ${
    delivery.deliverySpecificData.house
  }, кв. ${delivery.deliverySpecificData.apartment}, поч. инд. ${delivery.deliverySpecificData.postcode}`;
};

const sendMail = (body, receiverMail) => {
  const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: gmailUser,
      pass: gmailPass
    }
  });
  const mailOptions = {
    from: 'carefulstore.office@gmail.com',
    to: receiverMail,
    subject: 'Заказ в интернет магазине Careful Store',
    html: body
  };
  transporter.sendMail(mailOptions, function(err, info) {
    transporter.close();
    if (err) console.log(err);
    else console.log(info);
  });
};

const sendServiceMail = body => {
  const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: gmailUser,
      pass: gmailPass
    }
  });
  const mailOptions = {
    from: 'carefulstore.office@gmail.com',
    to: 'carefulstore.notification.center@gmail.com',
    subject: 'Новый заказ в магазине Careful Store',
    html: body
  };
  transporter.sendMail(mailOptions, function(err, info) {
    transporter.close();
    if (err) console.log(err);
    else console.log(info);
  });
};

module.exports = {
  sendServiceMail,
  sendMail,
  adressFromDeliveryObject,
  responseInvalidField,
  dbErrorHandler,
  responseSuccesful,
  responseNotSuccesful,
  responseMissingField,
  imageUrlFromHash,
  defaultGenderPhoto,
  chatIdFromUsersIds
};
