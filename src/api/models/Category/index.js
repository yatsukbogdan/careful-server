const mongoose = require('mongoose');
const { Schema } = mongoose;

const CategorySchema = new Schema({
  name: {
    type: String,
    required: true
  }
})

const responseObjectFromMongoInstance = category => {
  const _category = category.toObject();
  return ({
    id: _category._id,
    name: _category.name
  })
}

const Category = mongoose.model('Category', CategorySchema);

module.exports = { Category, responseObjectFromMongoInstance };