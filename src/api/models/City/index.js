const mongoose = require('mongoose');
const { Schema } = mongoose;

const CitySchema = new Schema({
  countryId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Country'
  },
  name: {
    type: String,
    required: true
  }
})

const responseObjectFromMongoInstance = city => {
  const _city = city.toObject();
  return ({
    id: _city._id,
    name: _city.name,
    countryId: _city.countryId
  })
}

const City = mongoose.model('City', CitySchema);

module.exports = { City, responseObjectFromMongoInstance };