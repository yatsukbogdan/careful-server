const mongoose = require('mongoose');
const { Schema } = mongoose;

const NovaPoshtaAdressSchema = new Schema({
  cityId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'City'
  },
  name: {
    type: String,
    required: true
  }
})

const responseObjectFromMongoInstance = point => {
  const _point = point.toObject();
  return ({
    id: _point._id,
    name: _point.name,
    cityId: _point.cityId
  })
}

const NovaPoshtaAdress = mongoose.model('NovaPoshtaAdress', NovaPoshtaAdressSchema);

module.exports = { NovaPoshtaAdress, responseObjectFromMongoInstance };