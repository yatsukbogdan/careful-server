const mongoose = require('mongoose');
const { imageUrlFromHash } = require('../../utils');
const { Schema } = mongoose;

const ProductSchema = new Schema({
  code: {
    type: String,
    required: true
  },
  currency: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  siteName: {
    type: String,
    required: true
  },
  purchasePrice: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  discountPrice: {
    type: Number,
    default: null
  },
  retailPrice: {
    type: Number,
    required: true
  },
  categoryId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Category'
  },
  album: {
    type: [
      {
        hash: {
          type: String,
          required: true
        },
        id: {
          type: String,
          required: true
        }
      }
    ],
    default: []
  },
  feedbacks: {
    type: [
      {
        name: {
          type: String,
          required: true
        },
        text: {
          type: String,
          required: true
        },
        visible: {
          type: Boolean,
          required: true
        },
        date: {
          type: Number,
          required: true
        },
        customerId: {
          type: Schema.Types.ObjectId,
          ref: 'Customer',
          default: null
        }
      }
    ],
    default: []
  },
  balance: {
    type: Number,
    default: 0
  }
});

const responseObjectFromMongoInstance = product => {
  const _product = product.toObject();
  return {
    id: _product._id,
    code: _product.code,
    name: _product.siteName,
    price: _product.retailPrice,
    album: _product.album.map(img => imageUrlFromHash(img.hash)),
    feedbacks: _product.feedbacks
      .filter(fb => fb.visible)
      .map(fb => ({
        name: fb.name,
        text: fb.text,
        date: fb.date
      })),
    balance: _product.balance,
    discountPrice: _product.discountPrice,
    currency: _product.currency,
    description: _product.description,
    categoryId: _product.categoryId
  };
};

const responseAdminImageObjectFromMongoInstance = img => ({ id: img.id, url: imageUrlFromHash(img.hash) });

const responseAdminObjectFromMongoInstance = product => {
  const _product = product.toObject();
  return {
    id: _product._id,
    code: _product.code,
    name: _product.name,
    siteName: _product.siteName,
    purchasePrice: _product.purchasePrice,
    retailPrice: _product.retailPrice,
    album: _product.album.map(responseAdminImageObjectFromMongoInstance),
    balance: _product.balance,
    feedbacks: _product.feedbacks.map(fb => ({
      id: fb._id,
      name: fb.name,
      text: fb.text,
      visible: fb.visible,
      date: fb.data,
      customerId: fb.customerId
    })),
    discountPrice: _product.discountPrice,
    currency: _product.currency,
    description: _product.description,
    categoryId: _product.categoryId
  };
};

const Product = mongoose.model('Product', ProductSchema);

module.exports = {
  Product,
  responseObjectFromMongoInstance,
  responseAdminObjectFromMongoInstance,
  responseAdminImageObjectFromMongoInstance
};
