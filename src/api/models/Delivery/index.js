const mongoose = require('mongoose');
const { Schema } = mongoose;

const DeliverySchema = new Schema({
  invoiceId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Invoice'
  },
  customerId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Customer'
  },
  method: {
    type: Number,
    required: true
  },
  countryId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Country'
  },
  declarationNumber: {
    type: String,
    default: null
  },
  sentToCustomer: {
    type: Number,
    default: null
  },
  city: {
    type: {
      name: {
        type: String,
        required: true
      },
      id: {
        type: String,
        required: true
      },
      source: {
        type: String,
        required: true
      }
    }
  },
  deliverySpecificData:
    {
      type: {
        streetId: {
          type: String,
          required: true
        },
        streetName: {
          type: String,
          required: true
        },
        house: {
          type: Number,
          require: true
        },
        apartment: {
          type: Number,
          require: true
        },
        postcode: {
          type: Number,
          require: true
        }
      }
    } |
    {
      type: {
        nppId: {
          type: String,
          required: true
        },
        nppName: {
          type: String,
          required: true
        }
      }
    }
});

const responseObjectFromMongoInstance = delivery => ({
  id: delivery._id,
  invoiceId: delivery.invoiceId,
  customerId: delivery.customerId,
  method: delivery.method,
  cityName: delivery.city.name,
  sentToCustomer: delivery.sentToCustomer,
  declarationNumber: delivery.declarationNumber,
  deliverySpecificData:
    delivery.method === 0
      ? {
          nppName: delivery.deliverySpecificData.nppName
        }
      : {
          streetName: delivery.deliverySpecificData.streetName,
          house: delivery.deliverySpecificData.house,
          apartment: delivery.deliverySpecificData.apartment,
          postcode: delivery.deliverySpecificData.postcode
        }
});

const Delivery = mongoose.model('Delivery', DeliverySchema);

module.exports = { Delivery, responseObjectFromMongoInstance };
