const mongoose = require('mongoose');
const { Schema } = mongoose;

const CustomerSchema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  }
});

const responseObjectFromMongoInstance = customer => ({
  id: customer._id,
  firstName: customer.firstName,
  lastName: customer.lastName,
  phone: customer.phone,
  email: customer.email
});

const Customer = mongoose.model('Customer', CustomerSchema);

module.exports = { Customer, responseObjectFromMongoInstance };
