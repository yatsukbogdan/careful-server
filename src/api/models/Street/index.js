const mongoose = require('mongoose');
const { Schema } = mongoose;

const StreetSchema = new Schema({
  cityId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'City'
  },
  name: {
    type: String,
    required: true
  }
})

const responseObjectFromMongoInstance = street => {
  const _street = street.toObject();
  return ({
    id: _street._id,
    name: _street.name,
    cityId: _street.cityId
  })
}

const Street = mongoose.model('Street', StreetSchema);

module.exports = { Street, responseObjectFromMongoInstance };