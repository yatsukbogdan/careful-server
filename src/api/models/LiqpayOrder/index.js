const mongoose = require('mongoose');
const { Schema } = mongoose;

const LiqpayOrderSchema = new Schema({
  paymentId: {
    type: Number,
    required: true
  },
  status: {
    type: String,
    required: true
  },
  liqpayOrderId: {
    type: String,
    required: true
  },
  amount: {
    type: Number,
    required: true
  },
  currency: {
    type: String,
    required: true
  },
  receiverCommission: {
    type: Number,
    required: true
  },
  createDate: {
    type: Number,
    required: true
  },
  transactionId: {
    type: Number,
    required: true
  },
  signature: {
    type: String,
    required: true
  }
});

const LiqpayOrder = mongoose.model('LiqpayOrder', LiqpayOrderSchema);

module.exports = { LiqpayOrder };
