const mongoose = require('mongoose');
const { Schema } = mongoose;

const InvoiceSchema = new Schema({
  createDate: {
    type: Number,
    required: true
  },
  type: {
    type: Number,
    required: true
  },
  products: {
    type: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          required: true
        },
        quantity: Number,
        price: Number
      }
    ],
    default: []
  },
  liqpayOrderId: {
    type: Schema.Types.ObjectId,
    default: null,
    ref: 'LiqpayOrder'
  },
  canceled: {
    type: Number,
    default: null
  }
});

const Invoice = mongoose.model('Invoice', InvoiceSchema);

const responseObjectFromMongoInstance = invoice => {
  let paid = null;
  const order = invoice.liqpayOrderId;
  if (order != null && (order.status === 'sandbox' || order.status === 'wait_accept' || order.status === 'success')) {
    console.log(order);
    paid = order.createDate;
  }
  return {
    id: invoice._id,
    createDate: invoice.createDate,
    type: invoice.type,
    paid,
    products: invoice.products.map(prod => ({
      id: prod.productId,
      quantity: prod.quantity,
      price: prod.price
    })),
    canceled: invoice.canceled
  };
};

module.exports = { Invoice, responseObjectFromMongoInstance };
