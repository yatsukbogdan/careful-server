const mongoose = require('mongoose');
const { Schema } = mongoose;

const CountrySchema = new Schema({
  name: {
    type: String,
    required: true
  }
})

const responseObjectFromMongoInstance = country => {
  const _country = country.toObject();
  return ({
    id: _country._id,
    name: _country.name
  })
}

const Country = mongoose.model('Country', CountrySchema);

module.exports = { Country, responseObjectFromMongoInstance };