const bodyParser = require('body-parser');
const express = require('express');
const passport = require('passport');
const { initializePassport } = require('./passport');
const cors = require('cors');
const { establishConnectionWithMongo } = require('./db');
const {
  userRouter,
  productRouter,
  countryRouter,
  cityRouter,
  streetRouter,
  novaPoshtaAdressRouter,
  liqpayRouter,
  categoryRouter,
  invoiceRouter,
  rozetkaRouter,
  customerRouter,
  deliveryRouter,
  adminRouter
} = require('./api/routes');

const app = express();

initializePassport();
establishConnectionWithMongo();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false, limit: '5mb' }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use('/images', express.static('images'));

app.use('/product', productRouter);
app.use('/country', countryRouter);
app.use('/category', categoryRouter);
app.use('/city', cityRouter);
app.use('/street', streetRouter);
app.use('/nova-poshta-adress', novaPoshtaAdressRouter);
app.use('/liqpay', liqpayRouter);
app.use('/rozetka', rozetkaRouter);
app.use('/invoice', invoiceRouter);
app.use('/customer', customerRouter);
app.use('/user', userRouter);
app.use('/delivery', deliveryRouter);
app.use('/admin', passport.authenticate('jwt', { session: false }), adminRouter);

const PORT = process.env.PORT || 4000;

app.listen(PORT, '127.0.01', () => console.log(`App listening on port ${PORT}`));
