const mongoose = require("mongoose");
const { mongoDbUrl } = require("./config");

module.exports = {
  establishConnectionWithMongo: () => mongoose.connect(mongoDbUrl)
}
