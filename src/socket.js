const jwt = require('jsonwebtoken');
const { User, Chat, Message } = require('./api/models');
const { chatIdFromUsersIds } = require('./api/utils');
const { responseMessageObjectFromMongoObject } = require('./api/models/Message');

const setupSocket = io => {
  io.on('connection', socket => {
    socket.on('authorize', (data, res) => onAuthorize(socket, data, res));
    socket.on('message', (data, res) => authorizationMiddlware(socket, data, res, onMessage));
    socket.on('readChat', (data, res) => authorizationMiddlware(socket, data, res, onReadChat));
    socket.on('disconnect', () => onDisconnect(socket));
  });
}

const successfulResponseObject = (payload) => {
  const object = { success: true };
  if (payload) object.payload = payload;
  return object;
}

const dbErrorHandler = (err, res) => {
  res(notSuccessfulResponseObject('Something went wrong with db'));
  console.log(err);
}

const missingFieldResponseObject = field => notSuccessfulResponseObject(`Missing field ${field}`)

const notSuccessfulResponseObject = (message, payload) => {
  const object = { success: false, message };
  if (payload) object.payload = payload;
  return object;
}

const authorizationMiddlware = (socket, data, res, callback) => {
  if (!data.token) return res(notSuccessfulResponseObject('No token in request'))
  const { id } = jwt.decode(data.token);

  if (!id) return res(notSuccessfulResponseObject('Invalid token'));
  
  delete data.token;

  User
    .findById(id)
    .then(user => {
      if (user.socketId !== socket.id) return res(notSuccessfulResponseObject('Socket not authorized'))
      callback(socket, { ...data, user }, res)
    })
    .catch(err => dbErrorHandler(err, res));
}


const onAuthorize = (socket, data, res) => {
  if (!data.token) return res(notSuccessfulResponseObject('No token in request'))
  const { id } = jwt.decode(data.token);
  if (!id) return res(notSuccessfulResponseObject('Invalid token'));
  User.findOneAndUpdate({ _id: id }, { socketId: socket.id, online: true }, (err, user)  => {
    if (err) return dbErrorHandler(err, res);
    res(successfulResponseObject())
    console.log(`User with id=${id} successfully authorized`)
  })
}

const onDisconnect = (socket) => {
  User.findOneAndUpdate(
    { socketId: socket.id },
    { socketId: null, online: false },
    (err, user)  => {
      console.log(`User with socketId=${socket.id} disconnected`)
    }
  )
}

const onMessage = (socket, data, res) => {
  if (!data.message) return res(missingFieldResponseObject('message'))
  const { user, message: { receiverId, payload, type }} = data;

  if (!receiverId) return res(missingFieldResponseObject('receiverId in message'))
  if (!payload) return res(missingFieldResponseObject('payload in message'))
  if (!type) return res(missingFieldResponseObject('type in message'))

  User
    .findById(receiverId)
    .then(partner => {
      if (!partner) return res(notSuccessfulResponseObject('There is no user with given id'))
      Message
        .create({
          timestamp: Date.now(),
          new: true,
          receiverId,
          payload,
          type
        })
        .then(message => {
          res(successfulResponseObject({ message: responseMessageObjectFromMongoObject(message) }));
          if (partner.socketId) socket.to(partner.socketId).emit('message', {
            partnerId: user.id,
            message: responseMessageObjectFromMongoObject(message)
          });
          const chatId = chatIdFromUsersIds(receiverId, user.id);
          Chat
            .findOne({ _id: chatId })
            .then(chat => {
              if (!chat) {
                Chat.create({
                  _id: chatId,
                  messagesIds: [message._id],
                  partnersIds: [receiverId, user.id]
                });
                return;
              }
              chat.messagesIds.push(message._id);
              chat.save();
            })
        })
    })
}

const onReadChat = (socket, data, res) => {
  if (!data.partnerId) return res(missingFieldResponseObject('chatId'))

  Chat
    .findOne({ _id: chatIdFromUsersIds(data.user.id, data.partnerId) })
    .populate('messagesIds')
    .then(chat => {
      if (!chat) return res(notSuccessfulResponseObject('There is no chat with given id'))
      chat.messagesIds.forEach(message => {
        if (message.receiverId == data.user.id) {
          message.new = false;
          message.save();
        }
      });
      User
        .findById(data.partnerId)
        .then(partner => {
          if (partner && !partner.socketId) return;
          socket.to(partner.socketId).emit('readChat', { partnerId: data.user.id });
        })
        .catch(err => dbErrorHandler(err, res))
      res(successfulResponseObject());
    })
    .catch(err => dbErrorHandler(err, res))
}

module.exports = { setupSocket }