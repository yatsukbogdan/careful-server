const { establishConnectionWithMongo } = require('./src/db');
establishConnectionWithMongo();
const { Country, Category } = require('./src/api/models');

const country = {
  name: 'Украина'
}

Country.create(country).then(country => console.log(country));

const category1 = {
  name: 'Сумки'
}

const category2 = {
  name: 'Авоськи'
}

const category3 = {
  name: 'Мешочки'
}

const category4 = {
  name: 'Гигиена'
}

const category5 = {
  name: 'Столовые приборы'
}


Category.create(category1).then(cat => console.log(cat))
Category.create(category2).then(cat => console.log(cat))
Category.create(category3).then(cat => console.log(cat))
Category.create(category4).then(cat => console.log(cat))
Category.create(category5).then(cat => console.log(cat))