const { establishConnectionWithMongo } = require('./src/db');
establishConnectionWithMongo();
const {
  Country,
  City,
  Street,
  NovaPoshtaAdress,
  Product,
  Delivery,
  Customer,
  Invoice,
  Category,
  LiqpayOrder
} = require('./src/api/models');

// Country.collection.drop();
// Country.collection.drop();
// City.collection.drop();
// Street.collection.drop();
// NovaPoshtaAdress.collection.drop();
// Product.collection.drop();
// Category.collection.drop();
Product.find({}).then(products => {
  products.forEach(prod => {
    prod.balance = 0;
    prod.save();
  });
});
LiqpayOrder.collection.drop();
Delivery.collection.drop();
Customer.collection.drop();
Invoice.collection.drop();
