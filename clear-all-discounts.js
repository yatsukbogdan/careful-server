const { establishConnectionWithMongo } = require('./src/db');
establishConnectionWithMongo();
const { Product } = require('./src/api/models');

Product.find({}).then(products => {
  products.forEach(prod => {
    prod.discountPrice = null;
    prod.save();
  });
});
