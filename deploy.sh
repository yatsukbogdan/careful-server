#!/bin/bash
DEPLOYHOST="root@138.197.185.68"

ssh -t $DEPLOYHOST "
  cd /var/www/careful-server
  git fetch --all && git reset --hard && git pull
  yarn install
  pm2 startOrReload ecosystem.config.js
"