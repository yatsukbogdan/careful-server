const { establishConnectionWithMongo } = require('./src/db');
establishConnectionWithMongo();
const { Product } = require('./src/api/models');

Product.findByIdAndUpdate('5c769fd8ffdb7805756c5a7f', { balance: 4 })
  .then(res => console.log(res))
  .catch(err => console.log(err));
Product.findByIdAndUpdate('5c7ba786dd177c0a25236e3e', { balance: 5 })
  .then(res => console.log(res))
  .catch(err => console.log(err));
