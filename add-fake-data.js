const { establishConnectionWithMongo } = require('./src/db');
establishConnectionWithMongo();
const { Country, Product, Category } = require('./src/api/models');

const country = {
  name: 'Украина'
}

Country.create(country).then(country => console.log(country));

const category1 = {
  name: 'Сумки'
}

const category2 = {
  name: 'Авоськи'
}

const category3 = {
  name: 'Мешочки'
}

const category4 = {
  name: 'Гигиена'
}

const category5 = {
  name: 'Столовые приборы'
}

const commonPayload = {
  currency: "UAH",
  album: [],
  balance: 100,
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
}

const product1 = {
  ...commonPayload,
  code: "000001",
  name: "Мешочки белые",
  siteName: "Мешочки белые",
  purchasePrice: 50,
  retailPrice: 200,
  discountPrice: 190
}

const product2 = {
  ...commonPayload,
  code: "000002",
  name: "Мешочки черные",
  siteName: "Мешочки черные",
  purchasePrice: 50,
  retailPrice: 200,
  discountPrice: null
}

const product3 = {
  ...commonPayload,
  code: "000003",
  name: "Мешочки красные",
  siteName: "Мешочки красные",
  purchasePrice: 50,
  retailPrice: 200,
  discountPrice: 180
}

const product4 = {
  ...commonPayload,
  code: "000004",
  name: "Сумка белая",
  siteName: "Сумка белая",
  purchasePrice: 50,
  retailPrice: 200,
  discountPrice: null
}

const product5 = {
  ...commonPayload,
  code: "000005",
  name: "Сумка черная",
  siteName: "Сумка черная",
  purchasePrice: 50,
  retailPrice: 200,
  discountPrice: null
}

const product6 = {
  ...commonPayload,
  code: "000006",
  name: "Сумка красная",
  siteName: "Сумка красная",
  purchasePrice: 50,
  retailPrice: 200,
  discountPrice: 180
}

const product7 = {
  ...commonPayload,
  code: "000007",
  name: "Авоська белая",
  siteName: "Авоська белая",
  purchasePrice: 50,
  retailPrice: 200,
  discountPrice: 190
}

const product8 = {
  ...commonPayload,
  code: "000008",
  name: "Авоська черная",
  siteName: "Авоська черная",
  purchasePrice: 50,
  retailPrice: 200,
  discountPrice: null
}

const product9 = {
  ...commonPayload,
  code: "000009",
  name: "Авоська красная",
  siteName: "Авоська красная",
  purchasePrice: 50,
  retailPrice: 200,
  discountPrice: 180
}


Category.create(category1).then(cat => {
  console.log(cat);
  product1.categoryId = cat._id;
  product2.categoryId = cat._id;
  product3.categoryId = cat._id;
  Product.create(product1).then(prod => console.log(prod));
  Product.create(product2).then(prod => console.log(prod));
  Product.create(product3).then(prod => console.log(prod));
})



Category.create(category2).then(cat => {
  console.log(cat);
  product4.categoryId = cat._id;
  product5.categoryId = cat._id;
  product6.categoryId = cat._id;
  Product.create(product4).then(prod => console.log(prod));
  Product.create(product5).then(prod => console.log(prod));
  Product.create(product6).then(prod => console.log(prod));
})


Category.create(category3).then(cat => {
  console.log(cat);
  product7.categoryId = cat._id;
  product8.categoryId = cat._id;
  product9.categoryId = cat._id;
  Product.create(product7).then(prod => console.log(prod));
  Product.create(product8).then(prod => console.log(prod));
  Product.create(product9).then(prod => console.log(prod));
})



Category.create(category4).then(cat => {
  console.log(cat);
  product4.categoryId = cat._id;
  product5.categoryId = cat._id;
  product6.categoryId = cat._id;
  Product.create(product4).then(prod => console.log(prod));
  Product.create(product5).then(prod => console.log(prod));
  Product.create(product6).then(prod => console.log(prod));
})


Category.create(category5).then(cat => {
  console.log(cat);
  product7.categoryId = cat._id;
  product8.categoryId = cat._id;
  product9.categoryId = cat._id;
  Product.create(product7).then(prod => console.log(prod));
  Product.create(product8).then(prod => console.log(prod));
  Product.create(product9).then(prod => console.log(prod));
})