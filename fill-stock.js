const { establishConnectionWithMongo } = require('./src/db');
establishConnectionWithMongo();
const { Product } = require('./src/api/models')

Product.find().then(prods => prods.forEach(prod => {
  prod.balance = 100;
  prod.save();
}))