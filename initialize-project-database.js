const { establishConnectionWithMongo } = require('./src/db');
establishConnectionWithMongo();
const { User } = require('./src/api/models');

const user1 = {
  email: 'evgpakhomova@gmail.com',
  passwordHash: 'e9e208e07c454d4531ba6f6e406775e2'
};

const user2 = {
  email: 'yatsukbogdan@gmail.com',
  passwordHash: '623b4e3c0dec60efba8054a243ed3715'
};

User.create(user1);
User.create(user2);
